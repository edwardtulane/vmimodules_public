# -*- coding: utf-8 -*-
"""
Created on Thu Feb 12 16:19:01 2015

@author: felix

Classes for VMI images

STATUS 2015-02-12
DONE:
TODO:

"""
from __future__ import absolute_import, division, print_function, unicode_literals


import sys, os, warnings

import numpy as np

import scipy.ndimage as ndimg
import scipy.integrate as integ
import scipy.ndimage.interpolation as ndipol
import scipy.optimize as opt
import scipy.signal as sig

from . import proc as vmp
from . import inv as vminv

try:
    import bottleneck as bn
except ImportError as er:
    print('Could not find bottleneck. Using numpy instead.')
    bn = np
    def ss(arr):
        return np.power(arr, 2).sum()
    bn.ss = ss

from . import mod_home
stor_dir = os.path.join(mod_home, 'storage')

mm_to_fs = 6671.28190396304

class RawImage(np.ndarray):
    """
    VMI class for reading and manipulating single frames
    Contains methods for orienting and interpolating a single frame,
    also operator overloading for manipulations between images

    Methods:
    -- crop_square: cuts out a square and may be supplied with the rotation angle
    """
    
    def __new__(self, file=[], xcntr=0, ycntr=0, radius=0, hotspots=[]):

        if type(file) is str:
            raw = vmp.rawread(file)
        else:
            raw = file

        if hotspots:
            raw = vmp.hot_spots(raw, hotspots)
        else:
            pass

        return np.ndarray.__new__(self, shape=raw.shape, dtype='int32',
                                  buffer=raw.copy(), order='C')

    def __init__(self, file=[], xcntr=0, ycntr=0, radius=0, hotspots=[]):

        self.cx, self.cy = xcntr, ycntr
        self.rad_sq = radius

        if not self.cx or not self.cy:
            self.cy, self.cx = (np.asarray(self.shape) - 1) / 2
            warnings.warn('No valid center given. Using (%d, %d)' % (self.cx,
                          self.cy))

        if not radius:
            size = self.shape
            dx, dy = np.min([self.cx, size[1] - self.cx - 1]), np.min([self.cy,
                             size[0] - self.cy - 1])
            self.rad_sq = np.min([dx, dy])
            warnings.warn('No valid radius given. Using %d' % (self.rad_sq))

    def crop_square(self, offset=0):
        cropd = vmp.centre_crop(self, self.cx, self.cy, self.rad_sq)
        return Frame(cropd, offset)

#==============================================================================

class Frame(np.ndarray):
    """
    Manipulation of square images incl. interpolation and rotation
    Arguments: frame array, offset angle
    Methods:
    -- interpol(sm)
    -- eval_rect(dens, disp, phi)
    -- eval_polar(radN, polN)
    -- centre_pbsx(cntr, ang, dens)
    deprecated:
    (-- rad_dist)
    (-- find_centre)
    """

    def __new__(self, frame, offs=0):

        size = frame.shape

        return np.ndarray.__new__(self, shape=size, buffer=frame.copy().data,
                                  dtype=frame.dtype.name, order='C')

    def __init__(self, frame, offs=0):

        size = frame.shape
        self.cy, self.cx = (np.asarray(frame.shape) - 1) / 2
        dx, dy = np.min([self.cx, size[1] - self.cx - 1]), np.min([self.cy,
                         size[0] - self.cy - 1])
        self.rad_sq = np.min([dx, dy])
        self.diam = size[0]
        self.offset = offs
        self.disp = np.array([0.0, 0.0, 0.0])

        self.interpol()

###

    def interpol(self, smooth=0.0):
        """ Cubic spline interpolation with zero smoothing """
        self.ck = sig.cspline2d(self, smooth)

    def __rotateframe(self, phi=0):
        """ Rotate the coefficient matrix in degs, cutting off the corners 
            2015-12-09: This routine introduces an undesired offset! 
            DO NOT USE """
        if not phi:
            self.ck = self.__ck
        else:
            self.ck = ndimg.rotate(self.__ck, phi,
                                   reshape=False, prefilter=False)

    def eval_rect(self, density, displace=[0., 0.], phi=0):
        """Deprecated. Superseded by eval_cart."""
        return self.eval_cart(density, displace, phi)

    def eval_cart(self, density, displace=[0., 0.], phi=0):
        """ Project the image onto a rectangular grid with given spacing """
        coords = vmp.gen_rect(self.diam, density, self.disp[1:] + displace, 
                              phi = self.offset + self.disp[0] + phi)
        rect = ndipol.map_coordinates(self.ck, coords, prefilter=False,
                                      output=np.float_)
        return CartImg(rect)

    def eval_polar(self, radN=251, polN=1025):
        """ Project the image onto a polar grid with radial and polar denss."""
        coords = vmp.gen_polar(self.rad_sq, radN, polN, self.disp[1:],
                              phi = self.offset + self.disp[0])
        polar = ndipol.map_coordinates(self.ck, coords, prefilter=False)
        return PolarImg(polar)

### Finding the centre point and offset angle

    def __sym_objective(self, disp, qsel):
        img = self.eval_cart(self.diam, disp[1:], phi=disp[0])
        img = vmp.crop_circle(img, (self.diam -1) / 2)
        qu = vmp.quadrants(img)
        qm = qu[qsel].mean(0)
        return np.power(qm[None,:,:] - qu, 2).sum()


    def find_centre(self, cntr=True, ang=True, qsel= [0,1,2,3]):
        init_vec = [0, 0, 0]

        if not cntr:
            domain[1:] = 0.0
        if not ang:
            domain[0] = 0.0

        res = opt.minimize(self.__sym_objective, init_vec, args=(qsel),
                           method='Nelder-Mead', #method='L-BFGS-B', bounds=domain,#'L-BFGS-B'
                           tol=1E-5, options={'disp': True})
        if res.success:
            return res
        else:
            warnings.warn('Centering failed!')

#===============================================================================

class CartImg(Frame):
    """
    Processed image after interpolation and recasting.
    Methods:
    -- TODO: return (weighted) quadrant(s)
    -- find_bg_factor: early stage of bg subtraction improvement
    """
    def __new__(self, frame):
        size = frame.shape[-2:]

        return np.ndarray.__new__(self, shape=frame.shape, dtype=np.float_,
                                  buffer=frame.copy().data, order='C')

    def __init__(self, frame):
        Frame.__init__(self, frame)

    def quadrants(self):
        return vmp.quadrants(self)

    def rad_dist(self, radN, polN=257, order=8):
        qu = vmp.quadrants(self)
        rd = np.zeros([4, (order/2)+1, radN])
        for i, q in enumerate(qu):
            rd[i] = vmp.get_raddist(q, radN, polN, order)
        return rd


    def __eval_bg_fac(self, fac, bg, inv):
        frame = self - fac * bg
        frame[frame < 0] = 0.

        quads = vmp.quadrants(frame)
        pb = np.zeros(quads.shape)
        for k, img in enumerate(quads):
            pb[k] = inv.invertMaxEnt(img)[2]
        dev = bn.ss(pb)
        return dev


    def find_bg_fac(self, bg, dens=501):
        """ Subtract a background image with an optimised factor """
        init_vec = [0.5]
        rad = (dens - 1) / 2
        inv = vminv.Inverter(rad, 8, dryrun=0)
        domain = [[0, 2]]
        self.bg_fac = opt.minimize(self.__eval_bg_fac, init_vec, args=(bg, inv),
                                   method='L-BFGS-B', bounds=domain,#'L-BFGS-B'
                                   tol=1E-5, options={'disp': True})
        if self.bg_fac.success:
            print('Found optimum factor: ', self.bg_fac.x)
        del inv
        return self.bg_fac.x, self.bg_fac.fun

#==============================================================================

class PolarImg(np.ndarray):
    """
    This is still a dummy for manipulations in polar coords.
    """
    def __new__(self, frame):

        size = frame.shape

        return np.ndarray.__new__(self, shape=size, dtype=np.float_,
                                  buffer=frame.copy().data, order='C')
    def __init__(self, frame):
        self.cy, self.cx = (0, 0)
        self.drad, self.dphi = frame.shape

    def quadrants(self):
        quad_phi = (self.dphi - 1) / 4
        qu = [self[:,0:quad_phi+1], self[:,2*quad_phi:quad_phi-1:-1],
              self[:,2*quad_phi:3*quad_phi+1], self[:,4*quad_phi:3*quad_phi-1:-1]]

        return np.array(qu)

    def get_cos2(self, qsel=[0,1,2,3]):
        qu = self.quadrants()
        qu = qu.mean(0)
        th = np.linspace(0, np.pi * 0.5, qu.shape[1])
        costh = np.cos(th)
        sinth = np.sin(th)

        lo = qu * sinth
        up = lo * costh ** 2
        return integ.romb(up, axis=1) / integ.romb(lo, axis=1)
        
