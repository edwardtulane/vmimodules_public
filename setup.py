from setuptools import setup, Extension
from platform import system

if system() == 'Linux':
	abel = Extension('vmimodules.inv.abel', ['vmimodules/inv/legendre/legendre_polynomial.c',
											 'vmimodules/inv/legendre/abel.c']
					)
	abel = [abel]
elif system() == 'Windows':
	abel = None
	
setup(
  name = 'vmimodules',
  version='0.8',
  ext_modules = abel,
  packages = ['vmimodules'],
# package_dir = {'': 'lib'},
# include_dirs=[numpy.get_include()]
)
