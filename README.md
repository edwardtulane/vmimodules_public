# Overview #

VMIModules is a collection of routines for manipulating and analysing data 
that were recorded witha Velocity Map Imaging spectrometre. 

## Features ##

* centering, rotation, and interpolation of VMI images
* Abel inversion with several different methods (Fourier-Hankel 
  transformation, BASEX, polar Basex, Maximum Entropy Reconstruction)
* extraction of radial and angular distributions of the inverted images

# Installation #

On Linux, the compilation and installation process should work out of the box if you have Cython and a C compiler installed.
On a Windows machine, the building process is a little more awkward because you need a specific compiler for every Python version.
For the details, please have a look at [this site](https://wiki.python.org/moin/WindowsCompilers).
I made it work with the anaconda Python3 distribution and the Microsoft Visual Studio 2015 Community Edition.

## Dependencies ##

(Numbers in brackets indicate the versions I tested this release with. Older versions will almost definitely work, as well.)

* Python 3.5 (should be backwards compatible to 2.7)
* NumPy (1.11)
* SciPy (0.17)
* Cython (0.24.1)
* bottleneck (1.1, optional)
* Jupyter Notebook + IPython kernel (4.2, for the documentation) 

## Manual installation with git ##

    git clone https://edwardtulane@bitbucket.org/edwardtulane/vmimodules_public.git vmimodules
    cd vmimodules
    python setup.py develop

Using the `develop` command has the advantage that you can modify the files inplace and generate basis sets without having root access.

# Documentation #

Have a look at the `manual.ipynb` file in the `doc/` folder that shows how to use this package.
For viewing and executing it, you need to start the Jupyter Notebook server that ships with anaconda. 
On Linux, you may have to install it from your distribution's repositories.
Instead of a proper testing suite, just execute all the cells and compare the output to the `manual.html`, which I generated from the notebook on my machine.